package main

import (
	"context"
	"fmt"
	"gitlab.com/samisgit/jobscrape/dispatcher"
	"gitlab.com/samisgit/jobscrape/scraper"
	"net/url"
	"os"
)

var blockList = []string{
	"chicago",
	"diego",
}

var requiredList = []string{
	"go",
}

func main(){
	ctx := context.Background()

	notificationURL := os.Getenv("NOTIFICATION_URL")
	if notificationURL == "" {
		panic(fmt.Errorf("expected NOTIFICATION_URL env var to be populated").Error())
	}

	gitlabURL, err := url.Parse("https://boards.greenhouse.io/gitlab")
	if err != nil {
		panic(err)
	}

	gs := scraper.GitlabService{
		ListingPage: gitlabURL,
	}

	jobs, err := gs.Scrape(ctx)
	if err != nil {
		panic(err)
	}

	filteredJobs := make([]dispatcher.Dispatchee, 0)
	for _, currentJob := range jobs {
		if currentJob.PassesConditions(blockList, requiredList) {
			filteredJobs = append(filteredJobs, currentJob)
		}
	}

	slackURL, err := url.Parse(notificationURL)
	if err != nil {
		panic(err)
	}
	sc := dispatcher.SlackClient{WebHookURL: slackURL}

	jobCount := len(filteredJobs)
	if jobCount > 3 {
		err = sc.Publish(ctx, filteredJobs[0:2])
		if err != nil {
			panic(err)
		}

		return
	}

	err = sc.Publish(ctx, filteredJobs)
	if err != nil {
		panic(err)
	}
}
