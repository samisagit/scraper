package job

import (
	"net/url"
	"strings"
)

type Job struct {
	Bio string
	URL *url.URL
	Location string
}

func (j *Job) GetBio() string {
	return j.Bio
}

func (j *Job) GetURL() *url.URL {
	return j.URL
}

func (j *Job) GetLocation() string {
	return j.Location
}

func (j *Job) PassesConditions(blockList, requiredList []string) bool {
	subject := strings.ToLower(j.Bio)

	filteredSubject := filterBlockList(subject, blockList)

	for _, requiredItem := range requiredList {
		if strings.Contains(filteredSubject, requiredItem) {
			return true
		}
	}

	return false
}

func filterBlockList(input string, blockList[]string) string {
	parts := strings.Split(input, "")
	for _, blockItem := range blockList {
		for strings.Contains(input, blockItem) {
			i := strings.Index(input, blockItem)
			length := len(blockItem)
			parts = append(parts[:i], parts[i+length:]...)
			input = strings.Join(parts, "")
		}
	}

	return input
}