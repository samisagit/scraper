package dispatcher

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type Dispatchee interface {
	GetBio() string
	GetLocation() string
	GetURL() *url.URL
}

type publisher interface {
	Publish(ctx context.Context, d []Dispatchee) error
}

type slackBlock struct {
	Type string `json:"type"`
	Text interface{} `json:"text,omitempty"`
	Elements interface{} `json:"elements,omitempty"`
}

type slackMessage struct {
	Blocks []slackBlock `json:"blocks"`
}

func (sm *slackMessage) mapJobs(d []Dispatchee) error {
	jobCount := len(d)

	sm.Blocks = append(sm.Blocks, slackBlock{
		Type: "section",
		Text: &slackBlock{
			Type: "mrkdwn",
			Text: fmt.Sprintf("I found %d jobs on gitlab.com :fox_face: take a look", jobCount),
		},
	})

	lessThanThree := jobCount < 3
	limiter := 3
	if lessThanThree {
		limiter = jobCount
	}

	if limiter == 0 {
		return nil
	}

	for _, line := range d[0:limiter] {
		sm.Blocks = append(sm.Blocks, slackBlock{
			Type: "divider",
		})

		sm.Blocks = append(sm.Blocks, slackBlock{
			Type: "section",
			Text: &slackBlock{
				Type:       "mrkdwn",
				Text:       fmt.Sprintf("Bio: %s", line.GetBio()),
			},
		})

		sm.Blocks = append(sm.Blocks, slackBlock{
			Type: "section",
			Text: &slackBlock{
				Type:       "mrkdwn",
				Text:       line.GetURL().String(),
			},
		})

		sm.Blocks = append(sm.Blocks, slackBlock{
			Type: "context",
			Elements: []*slackBlock{
				{
					Type:       "mrkdwn",
					Text:       fmt.Sprintf("Location: %s", line.GetLocation()),
				},
			},
		})
	}

	return nil
}

type SlackClient struct {
	WebHookURL *url.URL
}

func (sc SlackClient) Publish(ctx context.Context, d []Dispatchee) error {
	message := &slackMessage{}

	err := message.mapJobs(d)
	if err != nil {
		return err
	}

	m, err := json.Marshal(&message)
	if err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", fmt.Sprintf(sc.WebHookURL.String()), bytes.NewBuffer(m))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	_, err = client.Do(req)

	return err
}