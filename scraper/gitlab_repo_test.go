package scraper

import (
	"context"
	"gitlab.com/samisgit/jobscrape/job"
	"gitlab.com/samisgit/jobscrape/testingUtils"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
)

func parseHTMLDangerously(reader io.Reader) *html.Node {
	node, _ := html.Parse(reader)

	return node
}

func parseURLDangerously(input string) *url.URL {
	u, _ := url.Parse(input)

	return u
}

func openFileDangerously(path string) *os.File {
	f, _ := os.Open(path)

	return f
}

const validJobNode = `<div class="opening" department_id="4011044002,4011053002" office_id="" data-department-4011044002="true" data-department-4011053002="true">
    <a data-mapped="true" href="/gitlab/jobs/4225939002">Support Engineer, Americas Central</a>
    <br/>
    <span class="location">Remote, Americas, Mountain/Central Timezones</span>
</div>`

const invalidJobNode = `<div class="opening" department_id="4011044002,4011053002" office_id="" data-department-4011044002="true" data-department-4011053002="true">
    <br/>
    <span class="location">Remote, Americas, Mountain/Central Timezones</span>
</div>`

func TestGitlabServiceInterpretNode(ot *testing.T) {
	cases := []struct{
		name string
		inputNode *html.Node
		output *job.Job
		expectedError error
	}{
		{
			"valid node",
			parseHTMLDangerously(strings.NewReader(validJobNode)),
			&job.Job{
				Bio:      "Support Engineer, Americas Central",
				URL:      parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4225939002"),
				Location: "Remote, Americas, Mountain/Central Timezones",
			},
			nil,
		},
		{
			"invalid node",
			parseHTMLDangerously(strings.NewReader(invalidJobNode)),
			nil,
			InvalidJobNode,
		},
	}

	for _, val := range cases {
		ot.Run(val.name, func(t *testing.T) {
			gs := GitlabService{
				ListingPage: parseURLDangerously("https://boards.greenhouse.io"),
			}

			j, err := gs.interpretNode(val.inputNode)
			testingUtils.CheckErr(t, val.expectedError, err)

			if val.expectedError == nil && err == nil {
				if j.Location != val.output.Location {
					t.Errorf("expected location to be %s, got %s", val.output.Location, j.Location)
				}

				if j.Bio != val.output.Bio {
					t.Errorf("expected bio to be %s, got %s", val.output.Bio, j.Bio)
				}

				if j.URL.String() != val.output.URL.String() {
					t.Errorf("expected GetURL to be %s, got %s", val.output.URL.String(), j.URL.String())
				}
			}
		})
	}
}

func TestGitlabServiceTransform(ot *testing.T) {
	cases := []struct{
		name string
		input io.Reader
		output []*job.Job
		expectedError error
	}{
		{
			"valid document",
			openFileDangerously("./gitlabFakes/simplifiedJobBoard.html"),
			[]*job.Job{
				{
					Bio: "Engineering Manager, Delivery",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4574546002"),
					Location: "Remote",
				},
				{
					Bio: "Senior Product Designer or Product Designer, Manage: Compliance",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4613933002"),
					Location: "Remote",
				},
				{
					Bio: "Senior UX Researcher or UX Researcher, Manage",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4654965002"),
					Location: "Remote",
				},
			},
			nil,
		},
		{
			"valid but empty document",
			openFileDangerously("./gitlabFakes/emptyJobBoard.html"),
			nil,
			nil,
		},
		{
			"invalid document",
			openFileDangerously("./gitlabFakes/invalidJobBoard.csv"),
			nil,
			nil,
		},
	}

	for _, val := range cases {
		ot.Run(val.name, func(t *testing.T) {
			gs := GitlabService{
				ListingPage: parseURLDangerously("https://boards.greenhouse.io"),
			}

			jobs, err := gs.transform(val.input)
			testingUtils.CheckErr(t, val.expectedError, err)

			if len(jobs) != len(val.output) {
				t.Errorf("expected %d jobs, got %d", len(val.output), len(jobs))

				t.FailNow()
			}

			for _, outputItem := range jobs {
				var matchFound bool

				for _, inputItem := range  val.output  {
					if inputItem.Location == outputItem.Location && inputItem.URL.String() == outputItem.URL.String() && inputItem.Bio == outputItem.Bio {
						matchFound = true
						break
					}
				}

				if !matchFound {
					t.Errorf("job missing, expected match for input item with value %v", outputItem)
				}
			}
		})
	}
}

func TestGitlabServiceScrape(ot *testing.T) {
	cases := []struct{
		name string
		httpResponseBody io.Reader
		output []*job.Job
		expectedError error
	}{
		{
			"happy path",
			openFileDangerously("./gitlabFakes/simplifiedJobBoard.html"),
			[]*job.Job{
				{
					Bio: "Engineering Manager, Delivery",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4574546002"),
					Location: "Remote",
				},
				{
					Bio: "Senior Product Designer or Product Designer, Manage: Compliance",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4613933002"),
					Location: "Remote",
				},
				{
					Bio: "Senior UX Researcher or UX Researcher, Manage",
					URL: parseURLDangerously("https://boards.greenhouse.io/gitlab/jobs/4654965002"),
					Location: "Remote",
				},
			},
			nil,
		},
	}

	for _, val := range cases {
		ot.Run(val.name, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				_, err := io.Copy(w, val.httpResponseBody)
				if err != nil {
					t.Error(err)

					t.FailNow()
				}
			}))
			defer ts.Close()

			URLString := ts.URL
			gs := GitlabService{ListingPage: parseURLDangerously(URLString)}

			jobs, err := gs.Scrape(context.Background())
			testingUtils.CheckErr(t, val.expectedError, err)

			for _, outputItem := range jobs {
				var matchFound bool

				for _, inputItem := range  val.output  {
					// matching Path rather than full string, as the test server has it's own base GetURL
					if inputItem.Location == outputItem.Location && inputItem.URL.Path == outputItem.URL.Path && inputItem.Bio == outputItem.Bio {
						matchFound = true
						break
					}
				}

				if !matchFound {
					t.Errorf("job missing, expected match for input item with value %v", outputItem)
				}
			}
		})
	}
}