package scraper

import (
	"context"
	"fmt"
	"gitlab.com/samisgit/jobscrape/job"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"net/url"
	"time"
)

type GitlabService struct {
	ListingPage *url.URL
}

func (gs *GitlabService) Scrape(ctx context.Context) ([]*job.Job, error) {
	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	request, err := http.NewRequestWithContext(ctx, "GET", gs.ListingPage.String(), nil)
	if err != nil {
		return nil, err
	}
	request.Header.Set("User-Agent", "Daily individual job check bot")

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	jobs, err := gs.transform(response.Body)
	if err != nil {
		return nil, err
	}

	return jobs, nil
}

func (gs *GitlabService) transform(reader io.Reader) ([]*job.Job, error) {
	doc, err := html.Parse(reader)
	if err != nil {
		return nil, err
	}

	openings := make([]*html.Node, 0)

	var f func(*html.Node)
	f = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "div" {
			for _, a := range node.Attr {
				if a.Key == "class" && a.Val == "opening" {
					openings = append(openings, node)
				}
			}
		}
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	if len(openings) == 0 {
		return nil, nil
	}

	jobs := make([]*job.Job, len(openings))
	for i, opening := range openings {
		j, err := gs.interpretNode(opening)
		if err != nil {
			return nil, err
		}

		jobs[i] = j
	}

	return jobs, nil
}

func (gs *GitlabService) interpretNode(node *html.Node) (*job.Job, error) {
	j := &job.Job{}

	var linkFound, bioFound, locationFound bool

	var f func(*html.Node) error
	f = func(node *html.Node) error {
		if node.Type == html.ElementNode && node.Data == "a" {
			j.Bio = node.FirstChild.Data
			bioFound = true
			for _, a := range node.Attr {
				if a.Key == "href" {
					link, err := url.Parse(fmt.Sprintf("%s%s", gs.ListingPage, a.Val))
					if err != nil {
						return err
					}

					j.URL = link
					linkFound = true
				}
			}
		}
		if node.Type == html.ElementNode && node.Data == "span" {
			j.Location = node.FirstChild.Data
			locationFound = true
		}
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			if linkFound && bioFound && locationFound {
				return nil
			}

			f(c)
		}

		if !linkFound || !bioFound || !locationFound {
			return InvalidJobNode
		}
		return nil
	}
	err := f(node)
	if err != nil {
		return nil, err
	}

	return j, nil
}