package scraper

import (
	"context"
	"fmt"
	"gitlab.com/samisgit/jobscrape/job"
)

type Scraper interface {
	Scrape(ctx context.Context) ([]*job.Job, error)
}

var InvalidJobNode = fmt.Errorf("given node does not have sufficient information to create job.Job type")