package testingUtils

type ErrorerFailer interface {
	FailNow()
	Errorf(format string, args ...interface{})
}

func CheckErr(t ErrorerFailer, expectedErr, givenErr error) {
	if expectedErr != nil {
		if givenErr == nil {
			t.Errorf("expected call to return error with value '%s', got '%s'", expectedErr.Error(), "nil")

			t.FailNow()
		} else if expectedErr.Error() != givenErr.Error() {
			t.Errorf("expected call to return error with value '%s', got '%s'", expectedErr.Error(), givenErr.Error())

			t.FailNow()
		}
	} else {
		if givenErr != nil {
			t.Errorf("expected call to return nil error, got error with value '%s'", givenErr.Error())

			t.FailNow()
		}
	}
}