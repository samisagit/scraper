package testingUtils

import (
	"fmt"
	"testing"
)

type FakeErrorerFailer struct {
	FailNowCalled bool
	ErrorfCalled  bool
}

func (fef *FakeErrorerFailer) FailNow() {
	fef.FailNowCalled = true
}

func (fef *FakeErrorerFailer) Errorf(format string, args ...interface{}) {
	fef.ErrorfCalled = true
}

func TestCheckErr(ot *testing.T) {
	cases := []struct {
		name                  string
		expectedErr           error
		givenErr              error
		expectedFailNowCalled bool
		expectedErrorfCalled  bool
		fef                   *FakeErrorerFailer
	}{
		{
			name:                  "both nil",
			expectedErr:           nil,
			givenErr:              nil,
			expectedFailNowCalled: false,
			expectedErrorfCalled:  false,
			fef: &FakeErrorerFailer{
				FailNowCalled: false,
				ErrorfCalled:  false,
			},
		},
		{
			name:                  "matching errors",
			expectedErr:           fmt.Errorf("test error"),
			givenErr:              fmt.Errorf("test error"),
			expectedFailNowCalled: false,
			expectedErrorfCalled:  false,
			fef: &FakeErrorerFailer{
				FailNowCalled: false,
				ErrorfCalled:  false,
			},
		},
		{
			name:                  "non matching errors",
			expectedErr:           fmt.Errorf("test error with extra bits"),
			givenErr:              fmt.Errorf("test error"),
			expectedFailNowCalled: true,
			expectedErrorfCalled:  true,
			fef: &FakeErrorerFailer{
				FailNowCalled: false,
				ErrorfCalled:  false,
			},
		},
		{
			name:                  "unexpected error",
			expectedErr:           nil,
			givenErr:              fmt.Errorf("test error"),
			expectedFailNowCalled: true,
			expectedErrorfCalled:  true,
			fef: &FakeErrorerFailer{
				FailNowCalled: false,
				ErrorfCalled:  false,
			},
		},
		{
			name:                  "unexpected lack of error",
			expectedErr:           fmt.Errorf("test error"),
			givenErr:              nil,
			expectedFailNowCalled: true,
			expectedErrorfCalled:  true,
			fef: &FakeErrorerFailer{
				FailNowCalled: false,
				ErrorfCalled:  false,
			},
		},
	}

	for _, testCase := range cases {
		ot.Run(testCase.name, func(t *testing.T) {
			CheckErr(testCase.fef, testCase.expectedErr, testCase.givenErr)

			if testCase.fef.FailNowCalled != testCase.expectedFailNowCalled {
				t.Errorf(
					"FailNow() was called: %t, FailNow() expected to be called: %t",
					testCase.fef.FailNowCalled,
					testCase.expectedFailNowCalled,
				)
			}

			if testCase.fef.ErrorfCalled != testCase.expectedErrorfCalled {
				t.Errorf(
					"FailNow() was called: %t, FailNow() expected to be called: %t",
					testCase.fef.ErrorfCalled,
					testCase.expectedErrorfCalled,
				)
			}
		})
	}
}

